//
//  3DModelViewer.swift
//  ArkitDemo
//
//  Created by Alexander John on 26/10/21.
//

import UIKit
import ARKit

class ARDesignViewer: UIViewController {
    
    @IBOutlet weak var sceneView: ARSCNView!
    var designName = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        super.viewDidLoad()
        configureLighting()
        addModelDesign(fileName: designName)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let configuration = ARWorldTrackingConfiguration()
        sceneView.session.run(configuration)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        sceneView.session.pause()
    }
    
    
    
    func configureLighting() {
        sceneView.autoenablesDefaultLighting = true
        sceneView.automaticallyUpdatesLighting = true
    }
    
    func addModelDesign(x: Float = 0, y: Float = 0, z: Float = -0.2, fileName: String = "Dragon 2.5_dae.dae") {
        
        let fileNameComponents = fileName.split(separator: ".").map { String($0) }
        let fileExt = fileNameComponents.last
        
        var scnFileName = fileName
        if fileName.count > 4 && fileName.contains(".scn"){
            scnFileName.removeLast(4)
        }
        
        if fileExt == "scn"{
            guard let scnScene = SCNScene(named: fileName), let scnSceneNode = scnScene.rootNode.childNode(withName: scnFileName, recursively: true) else { return }
            scnSceneNode.position = SCNVector3(x, y, z)
            sceneView.scene.rootNode.addChildNode(scnSceneNode)
        }else if fileExt == "dae"{
            guard let modelScene = SCNScene(named: fileName) else { return }
            let modelNode = SCNNode()
            let modelSceneChildNodes = modelScene.rootNode.childNodes
            for childNode in modelSceneChildNodes {
                modelNode.addChildNode(childNode)
            }
            modelNode.position = SCNVector3(x, y, z)
            
            let scale = 0.009
            
            modelNode.scale = SCNVector3(scale, scale, scale)
            sceneView.scene.rootNode.addChildNode(modelNode)
        }else{
            print("invalid format")
        }
        
        
        
    }
    
    
    
}
