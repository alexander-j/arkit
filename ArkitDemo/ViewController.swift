//
//  ViewController.swift
//  ArkitDemo
//
//  Created by Alexander John on 26/10/21.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var designCollectionView: UICollectionView!
    let images = [#imageLiteral(resourceName: "dragon"), #imageLiteral(resourceName: "wolf"), #imageLiteral(resourceName: "cadnav-201201113934"), #imageLiteral(resourceName: "intergalactic"), #imageLiteral(resourceName: "Minecraft"), #imageLiteral(resourceName: "maxresdefault"), #imageLiteral(resourceName: "5256-low_poly_tree"), #imageLiteral(resourceName: "paperplane")]
    var designNames = ["dragon.dae", "wolf.dae", "F-15C_Eagle.dae", "Intergalactic_Spaceship.dae", "steve.dae", "Cyber truck.dae", "Lowpoly_tree_sample.dae", "paperPlane.scn"]
    override func viewDidLoad() {
        super.viewDidLoad()
        designCollectionView.delegate = self
        designCollectionView.dataSource = self
        
    }
}

extension ViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CustomCollectionViewCell", for: indexPath as IndexPath) as! CustomCollectionViewCell
        cell.designPreviewView.image = images[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfSections section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding: CGFloat =  15
        let collectionViewSize = designCollectionView.frame.size.width - padding
        let layout = designCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.sectionInset = UIEdgeInsets(top: 6, left: 4, bottom: 6, right: 4)
        layout.minimumInteritemSpacing = 15
        layout.minimumLineSpacing = 20
        layout.invalidateLayout()
        return CGSize(width: ((collectionViewSize / 2) - 6), height: ((collectionViewSize / 2) - 6))
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("dsadjashjk")
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let aRDesignViewer = storyboard.instantiateViewController(withIdentifier: "ARDesignViewer") as! ARDesignViewer
        aRDesignViewer.designName = designNames[indexPath.row]
        self.navigationController?.pushViewController(aRDesignViewer, animated: true)
    }
}
